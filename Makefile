#### PCM_memoryActivities ####
CXX=g++
CXXFLAGS += -Wall -g -O3 -Wno-unknown-pragmas -Ipcm -std=c++11
TARGET = pcm-memoryActivities
LIB = -pthread -lrt pcm/libPCM.a

.PHONY: all
all: $(TARGET) example
$(TARGET):$(TARGET).cpp
	$(CXX) $(CXXFLAGS) $< -o $@ $(LIB)
lib:
	cd pcm && git reset --hard "73067ae1c1063e865098928a60d74de131a0ed65"
	cp pcm/cpucounters.h pcm/cpucounters.h.bak
	patch -p0 < cpucounter.patch
	$(MAKE) -C pcm lib

example: example_IPC_client.cpp
	$(CXX) $(CXXFLAGS) $< -o $@_IPC_client
.PHONY: clean
clean: 
	rm -rf $(TARGET) example_IPC_client *.o
