/*
 Copyright (c) 2009-2017, Intel Corporation
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * Neither the name of Intel Corporation nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 Copyright (c) 2017, Seoul National University
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "pcm-memoryActivities.h"

#include <fstream>
#include <unistd.h>
#include <signal.h>
#include <math.h>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <sstream>
#include <assert.h>

#include "utils.h"
#include <map>
#include <stdlib.h>
#include <sys/mman.h>

using namespace std;

map<string, struct extension> eventMap;

string eventString[4];
string unmaskString[4];

int bank_flg = 0;
unsigned char mode = 0;

int s_max_pos, c_max_pos, r_max_pos, b_max_pos = 0;
int max_val = 0;

typedef struct extension {
  unsigned char eventCode;
  map<string, unsigned char> unmaskMap;
} EXTENSION;

void setEventMap() {
  struct extension temp;
  temp.eventCode = CAS_COUNT;
  eventMap.insert(pair<string, struct extension>("CAS_COUNT", temp));
  temp.eventCode = RD_CAS_RANK0;
  eventMap.insert(pair<string, struct extension>("RD_CAS_RANK0", temp));
  temp.eventCode = RD_CAS_RANK1;
  eventMap.insert(pair<string, struct extension>("RD_CAS_RANK1", temp));
  temp.eventCode = RD_CAS_RANK2;
  eventMap.insert(pair<string, struct extension>("RD_CAS_RANK2", temp));
  temp.eventCode = RD_CAS_RANK3;
  eventMap.insert(pair<string, struct extension>("RD_CAS_RANK3", temp));
  temp.eventCode = RD_CAS_RANK4;
  eventMap.insert(pair<string, struct extension>("RD_CAS_RANK4", temp));
  temp.eventCode = RD_CAS_RANK5;
  eventMap.insert(pair<string, struct extension>("RD_CAS_RANK5", temp));
  temp.eventCode = RD_CAS_RANK6;
  eventMap.insert(pair<string, struct extension>("RD_CAS_RANK6", temp));
  temp.eventCode = RD_CAS_RANK7;
  eventMap.insert(pair<string, struct extension>("RD_CAS_RANK7", temp));
  temp.eventCode = WR_CAS_RANK0;
  eventMap.insert(pair<string, struct extension>("WR_CAS_RANK0", temp));
  temp.eventCode = WR_CAS_RANK1;
  eventMap.insert(pair<string, struct extension>("WR_CAS_RANK1", temp));
  temp.eventCode = WR_CAS_RANK2;
  eventMap.insert(pair<string, struct extension>("WR_CAS_RANK2", temp));
  temp.eventCode = WR_CAS_RANK3;
  eventMap.insert(pair<string, struct extension>("WR_CAS_RANK3", temp));
  temp.eventCode = WR_CAS_RANK4;
  eventMap.insert(pair<string, struct extension>("WR_CAS_RANK4", temp));
  temp.eventCode = WR_CAS_RANK5;
  eventMap.insert(pair<string, struct extension>("WR_CAS_RANK5", temp));
  temp.eventCode = WR_CAS_RANK6;
  eventMap.insert(pair<string, struct extension>("WR_CAS_RANK6", temp));
  temp.eventCode = WR_CAS_RANK7;
  eventMap.insert(pair<string, struct extension>("WR_CAS_RANK7", temp));
}

void setUnmaskMap() {
  eventMap["CAS_COUNT"].unmaskMap.insert(
      pair<string, unsigned char>("RD", CAS_COUNT_RD));
  eventMap["CAS_COUNT"].unmaskMap.insert(
      pair<string, unsigned char>("WR", CAS_COUNT_WR));

  string ranknum[16];
  ranknum[0] = "RD_CAS_RANK0";
  ranknum[1] = "RD_CAS_RANK1";
  ranknum[2] = "RD_CAS_RANK2";
  ranknum[3] = "RD_CAS_RANK3";
  ranknum[4] = "RD_CAS_RANK4";
  ranknum[5] = "RD_CAS_RANK5";
  ranknum[6] = "RD_CAS_RANK6";
  ranknum[7] = "RD_CAS_RANK7";
  ranknum[8] = "WR_CAS_RANK0";
  ranknum[9] = "WR_CAS_RANK1";
  ranknum[10] = "WR_CAS_RANK2";
  ranknum[11] = "WR_CAS_RANK3";
  ranknum[12] = "WR_CAS_RANK4";
  ranknum[13] = "WR_CAS_RANK5";
  ranknum[14] = "WR_CAS_RANK6";
  ranknum[15] = "WR_CAS_RANK7";

  string tempEvent[2];

  //BANK loop
  for (int i = 0; i < 16; i++) {
    eventMap[ranknum[i]].unmaskMap.insert(
        pair<string, unsigned char>("BANK0", BANK0));
    eventMap[ranknum[i]].unmaskMap.insert(
        pair<string, unsigned char>("BANK1", BANK1));
    eventMap[ranknum[i]].unmaskMap.insert(
        pair<string, unsigned char>("BANK2", BANK2));
    eventMap[ranknum[i]].unmaskMap.insert(
        pair<string, unsigned char>("BANK3", BANK3));
    eventMap[ranknum[i]].unmaskMap.insert(
        pair<string, unsigned char>("BANK4", BANK4));
    eventMap[ranknum[i]].unmaskMap.insert(
        pair<string, unsigned char>("BANK5", BANK5));
    eventMap[ranknum[i]].unmaskMap.insert(
        pair<string, unsigned char>("BANK6", BANK6));
    eventMap[ranknum[i]].unmaskMap.insert(
        pair<string, unsigned char>("BANK7", BANK7));
  }

  //RANK loop
  for (int i = 0; i < 2; i++) {
    eventMap[tempEvent[i]].unmaskMap.insert(
        pair<string, unsigned char>("RANK0", RANK0));
    eventMap[tempEvent[i]].unmaskMap.insert(
        pair<string, unsigned char>("RANK1", RANK1));
    eventMap[tempEvent[i]].unmaskMap.insert(
        pair<string, unsigned char>("RANK2", RANK2));
    eventMap[tempEvent[i]].unmaskMap.insert(
        pair<string, unsigned char>("RANK3", RANK3));
    eventMap[tempEvent[i]].unmaskMap.insert(
        pair<string, unsigned char>("RANK4", RANK4));
    eventMap[tempEvent[i]].unmaskMap.insert(
        pair<string, unsigned char>("RANK5", RANK5));
    eventMap[tempEvent[i]].unmaskMap.insert(
        pair<string, unsigned char>("RANK6", RANK6));
    eventMap[tempEvent[i]].unmaskMap.insert(
        pair<string, unsigned char>("RANK7", RANK7));
  }
}

void print_help(char *prog_name) {
  cout << "Options:" << endl;
  cout << "  -M: Monitor mode" << endl;
  cout << "  -C: Capture mode" << endl;
  cout << "  -e: event name" << endl;
  cout << "  -u: unmask name" << endl;
  cout << "  -i: interval" << endl;

  cout << endl;
  cout << "  Monitor mode Example: sudo " << prog_name
       << " -M -e RD_CAS_RANK0 -u BANK0 -e RD_CAS_RANK0 -u BANK1" << endl;
  cout << "                        sudo " << prog_name
       << " -M -e CAS_COUNT -u WR -e CAS_COUNT -u RD" << endl;
  cout << "  Capture mode Example: sudo " << prog_name
       << " -C -e CAS_COUNT -u WR -e CAS_COUNT -u RD" << endl;
  cout << endl;
}

void PCM_MemoryActivities::display_count(float iMC_socket_chan[][4][8],
                                         float iMC_socket[][4],
                                         uint32 numSockets,
                                         uint32 num_imc_channels) {
  uint32 skt = 0;
  cout.setf(ios::fixed);
  cout.precision(2);

  cout << "Time elapsed: " << dec << fixed << AfterTime - BeforeTime << " ms\n";
  while (skt < numSockets) {
    if (!(skt % 2) && ((skt + 1) < numSockets)) {  //This is even socket, and it has at least one more socket which can be displayed together
      cout
          << "\
                \r--------------------------------------------------||--------------------------------------------------\n\
                \r--                  Socket " << skt << "                    --||--                   Socket " << skt + 1 << "                  --\n\
                \r--------------------------------------------------||--------------------------------------------------\n\
                \r--------------------------------------------------||--------------------------------------------------\n\
                \r--------------------------------------------------||--------------------------------------------------\n\
                \r--        Memory Performance Monitoring         --||--         Memory Performance Monitoring        --\n\
                \r--------------------------------------------------||--------------------------------------------------\n\
                \r";
      for (uint64 channel = 0; channel < num_imc_channels; ++channel) {
        if (iMC_socket_chan[0][skt][channel] < 0.0
            && iMC_socket_chan[1][skt][channel] < 0.0)  //If the channel read neg. value, the channel is not working; skip it.
          continue;

        cout << "\r--  Mem Ch " << channel;
        for (int ctr = 0; ctr < 2; ctr++) {
          switch (event[ctr]) {
            case CAS_COUNT:
            case RD_CAS_RANK0:
            case RD_CAS_RANK1:
            case RD_CAS_RANK2:
            case RD_CAS_RANK3:
            case RD_CAS_RANK4:
            case RD_CAS_RANK5:
            case RD_CAS_RANK6:
            case RD_CAS_RANK7:
            case WR_CAS_RANK0:
            case WR_CAS_RANK1:
            case WR_CAS_RANK2:
            case WR_CAS_RANK3:
            case WR_CAS_RANK4:
            case WR_CAS_RANK5:
            case WR_CAS_RANK6:
            case WR_CAS_RANK7:
              cout << "\r--  Mem Ch " << channel << ": CAS" << "_"
                  << unmaskString[ctr] << " " << ": Count (C/s):" << setw(10)
                  << iMC_socket_chan[ctr][skt][channel];
              cout << "   --||--  Mem Ch " << channel << ": CAS" << "_"
                  << unmaskString[ctr] << " " << ": Count (C/s):" << setw(10)
                  << iMC_socket_chan[ctr][skt + 1][channel] << "   --" << endl;
              break;
            default:
              cout << event[ctr] << " Event is not declared" << endl;
              break;
          }
          iMC_socket[ctr][skt] += iMC_socket_chan[ctr][skt][channel];
        }
      }
      skt += 2;
    } else {
      cout
          << "\
                \r-----------------------------------------------\n\
                \r--                 Socket " << skt << "                  --\n\
                \r-----------------------------------------------\n\
                \r-----------------------------------------------\n\
                \r-----------------------------------------------\n\
                \r--       Memory Performance Monitoring       --\n\
                \r-----------------------------------------------\n\
                \r";
      for (uint64 channel = 0; channel < num_imc_channels; ++channel) {
        if (iMC_socket_chan[0][skt][channel] < 0.0
            && iMC_socket_chan[1][skt][channel] < 0.0)  //If the channel read neg. value, the channel is not working; skip it.
          continue;

        cout << "\r--  Mem Ch " << channel;
        for (int ctr = 0; ctr < 2; ctr++) {
          switch (event[ctr]) {
            case CAS_COUNT:
            case RD_CAS_RANK0:
            case RD_CAS_RANK1:
            case RD_CAS_RANK2:
            case RD_CAS_RANK3:
            case RD_CAS_RANK4:
            case RD_CAS_RANK5:
            case RD_CAS_RANK6:
            case RD_CAS_RANK7:
            case WR_CAS_RANK0:
            case WR_CAS_RANK1:
            case WR_CAS_RANK2:
            case WR_CAS_RANK3:
            case WR_CAS_RANK4:
            case WR_CAS_RANK5:
            case WR_CAS_RANK6:
            case WR_CAS_RANK7:
              cout << "\r--  Mem Ch " << channel << ": bank " << ctr + bank_flg
                  << ": Count (C/s):" << setw(10)
                  << iMC_socket_chan[ctr][skt][channel];
              cout << "   --" << endl;
              break;
            default:
              cout << event[ctr] << " Event is not declared" << endl;
              break;
          }
          iMC_socket[ctr][skt] += iMC_socket_chan[ctr][skt][channel];
        }
      }

      skt += 2;
    }
  }
  swap(BeforeTime, AfterTime);
  swap(BeforeState, AfterState);
}

void PCM_MemoryActivities::analysis_iMC() {
  const uint32 num_imc_channels = m->getMCChannelsPerSocket();
  float iMC_socket_chan[4][max_sockets][max_imc_channels] = { };
  float iMC_socket[4][max_sockets] = { };
  uint64 partial_write[max_sockets];
  uint64 elapsedTime = AfterTime - BeforeTime;
  for (uint32 skt = 0; skt < m->getNumSockets(); ++skt) {
    iMC_socket[0][skt] = 0.0;
    iMC_socket[1][skt] = 0.0;
    iMC_socket[2][skt] = 0.0;
    iMC_socket[3][skt] = 0.0;
    partial_write[skt] = 0;

    for (uint32 channel = 0; channel < max_imc_channels; ++channel) {
      if (getMCCounter(channel, READ, BeforeState[skt], AfterState[skt]) == 0.0
          && getMCCounter(channel, WRITE, BeforeState[skt], AfterState[skt])
              == 0.0) {  //In case of JKT-EN, there are only three channels. Skip one and continue.
        iMC_socket_chan[0][skt][channel] = 0.0;
        iMC_socket_chan[1][skt][channel] = 0.0;
        iMC_socket_chan[2][skt][channel] = 0.0;
        iMC_socket_chan[3][skt][channel] = 0.0;
        continue;
      }

      for (int ctr = 0; ctr < 2; ctr++) {
        switch (event[ctr]) {
          case CAS_COUNT:
            iMC_socket_chan[ctr][skt][channel] = (float) (getMCCounter(
                channel, ctr, BeforeState[skt], AfterState[skt]));
            break;
          case RD_CAS_RANK0:
          case RD_CAS_RANK1:
          case RD_CAS_RANK2:
          case RD_CAS_RANK3:
          case RD_CAS_RANK4:
          case RD_CAS_RANK5:
          case RD_CAS_RANK6:
          case RD_CAS_RANK7:
          case WR_CAS_RANK0:
          case WR_CAS_RANK1:
          case WR_CAS_RANK2:
          case WR_CAS_RANK3:
          case WR_CAS_RANK4:
          case WR_CAS_RANK5:
          case WR_CAS_RANK6:
          case WR_CAS_RANK7:
            iMC_socket_chan[ctr][skt][channel] = (float) (getMCCounter(
                channel, ctr, BeforeState[skt], AfterState[skt]));
            break;
          default:
            break;
        }
        iMC_socket[ctr][skt] += iMC_socket_chan[ctr][skt][channel];

      }
      partial_write[skt] += (uint64)(
          getMCCounter(channel, PARTIAL, BeforeState[skt], AfterState[skt])
              / (elapsedTime / 1000.0));
    }
  }
  display_count(iMC_socket_chan, iMC_socket, m->getNumSockets(),
                num_imc_channels);
}

int main(int argc, char * argv[]) {
  cout << endl;
  cout
      << " Processor Counter Monitor: Memory Management Unit  Monitoring Utility "
      << PCM_VERSION << endl;
  cout << " This utility measures memory bandwidth per channel in real-time"
       << endl;
  cout << endl;
  PCM_IPCManager* pcmIPC = NULL;
  double delay = 1;
  int eventCount = 0;
  int unmaskCount = 0;

  setEventMap();
  setUnmaskMap();
  unsigned char * event = new unsigned char[4];
  unsigned char * unmask = new unsigned char[4];
  /* argument parsing */
  for (int i = 0; i < argc; i++) {
    if ((argc < 2 || argv[i] == string("-help")) || (argv[i] == string("-h"))
        || (argv[i] == string("-H"))) {
      cout
          << "------------------------------------------------------------------------------- "
          << endl;
      cout << " Memory Activities Analysis Tool for Intel processor" << endl;
      cout
          << " Modified by SCAL(Scalable Computer Architecture Laboratory), Seoul National University"
          << endl;
      cout << endl;
      print_help(argv[0]);
      return 0;
    } else if ((argv[i] == string("-M")) || (argv[i] == string("-monitor"))) {
      mode = 1;
    } else if ((argv[i] == string("-C")) || (argv[i] == string("-capture"))) {
      mode = 2;
    } else if ((argv[i] == string("-e")) || (argv[i] == string("-event"))) {
      i++;
      eventString[eventCount] = argv[i];
      event[eventCount] = eventMap[argv[i]].eventCode;
      eventCount++;
    } else if ((argv[i] == string("-u")) || (argv[i] == string("-unmask"))) {
      i++;
      unmaskString[unmaskCount] = argv[i];
      unmask[unmaskCount] =
          eventMap[eventString[eventCount - 1]].unmaskMap[argv[i]];
      unmaskCount++;
    } else if ((argv[i] == string("-i")) || (argv[i] == string("-interval"))) {
      delay = atof(argv[++i]);
      if (delay == -1.0)
        delay = 1;
    }
  }

  if (mode == 0) {
    print_help(argv[0]);
    return 0;
  } else if (mode == 2)
    pcmIPC = new PCM_IPCManager(PCM_SERVER);

  /* system checking */
  PCM_MemoryActivities pcm_memact(event, unmask);
  std::cout << "Update every " << delay << " seconds" << std::endl;

  /* Program start */
  static int loop_cnt = 0;

  cout.setf(ios::hex, ios::basefield);
  for (int i = 0; i < eventCount; i++)
    cout << "event " << i << ": " << eventString[i] << "[0x" << (int) event[i]
         << "]" << " unmask " << i << ": " << unmaskString[i] << "[0x"
         << (int) unmask[i] << "]" << endl;
  cout.unsetf(ios::hex);
  bank_flg = 0;

  if (mode == 2) {
    printf("\nipc ready\n");
    pcmIPC->pcm_wait(delay);
  }

  while (1) {
    bank_flg = 4;
    pcm_memact.programIMC();

    pcm_memact.PCM_before_getSystemCounterState();

    if (mode == 2) {
      pcmIPC->pcm_wait(delay);
    } else
      MySleepMs(int(delay * 1000));

    pcm_memact.PCM_after_getSystemCounterState();

    cout << "Mode:" << argv[1] << " loop_cnt: " << loop_cnt << " event: "
         << eventString[0];
    for (int i = 0; i < eventCount; i++)
      cout << " unmask " << i << ": " << unmaskString[i];
    cout << endl;
    pcm_memact.analysis_iMC();
    cout << endl;

    loop_cnt++;
  }
  return 0;
}
