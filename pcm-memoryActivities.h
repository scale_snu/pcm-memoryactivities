/*
 Copyright (c) 2009-2017, Intel Corporation
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * Neither the name of Intel Corporation nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 Copyright (c) 2017, Seoul National University
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef PCM_MEMORY_DETAIL_H_
#define PCM_MEMORY_DETAIL_H_

#include <sys/ipc.h>
#include <sys/shm.h>
#include <iostream>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include "pcm/cpucounters.h"

//Performance Monitor Events for iMC
#define CAS_COUNT     0x04
#define RD_CAS_RANK0  0xb0
#define RD_CAS_RANK1  0xb1
#define RD_CAS_RANK2  0xb2
#define RD_CAS_RANK3  0xb3
#define RD_CAS_RANK4  0xb4
#define RD_CAS_RANK5  0xb5
#define RD_CAS_RANK6  0xb6
#define RD_CAS_RANK7  0xb7
#define WR_CAS_RANK0  0xb8
#define WR_CAS_RANK1  0xb9
#define WR_CAS_RANK2  0xba
#define WR_CAS_RANK3  0xbb
#define WR_CAS_RANK4  0xbc
#define WR_CAS_RANK5  0xbd
#define WR_CAS_RANK6  0xbe
#define WR_CAS_RANK7  0xbf

//Unit Masks for CAS_COUNT
#define CAS_COUNT_RD  (3<<0)
#define CAS_COUNT_WR  (0xC<<0)

//Unit Masks for RANK
#define RANK0     (1<<0)
#define RANK1     (1<<1)
#define RANK2     (1<<2)
#define RANK3     (1<<3)
#define RANK4     (1<<4)
#define RANK5     (1<<5)
#define RANK6     (1<<6)
#define RANK7     (1<<7)

//Unit Masks for BANK
#define BANK0     (1<<0)
#define BANK1     (1<<1)
#define BANK2     (1<<2)
#define BANK3     (1<<3)
#define BANK4     (1<<4)
#define BANK5     (1<<5)
#define BANK6     (1<<6)
#define BANK7     (1<<7)

#define PCM_SERVER 0
#define PCM_CLIENT 1
#define WAIT_ON   1
#define WAIT_OFF  0

//Programmable iMC counter
#define READ 0
#define WRITE 1
#define PARTIAL 2

const uint32 max_sockets = 4;
const uint32 max_imc_channels = 8;

class PCM_MemoryActivities {
 public:
  PCM_MemoryActivities(unsigned char* _event, unsigned char* _unmask) {
    m = PCM::getInstance();
    m->disableJKTWorkaround();
    PCM::ErrorCode status = m->program();

    switch (status) {
      case PCM::Success:
        break;
      case PCM::MSRAccessDenied:
        std::cerr
            << "Access to Processor Counter Monitor has denied (no MSR or PCI CFG space access)."
            << std::endl;
        exit(EXIT_FAILURE);
      case PCM::PMUBusy:
        std::cerr
            << "Access to Processor Counter Monitor has denied (Performance Monitoring Unit is occupied by other application). Try to stop the application that uses PMU."
            << std::endl;
        std::cerr
            << "Alternatively you can try to reset PMU configuration at your own risk. Try to reset? (y/n): ";
        char yn;
        std::cin >> yn;
        if ('y' == yn) {
          m->resetPMU();
          std::cerr
              << "PMU configuration has been reset. Try to rerun the program again."
              << std::endl;
        }
        exit(EXIT_FAILURE);
      default:
        std::cerr
            << "Access to Processor Counter Monitor has denied (Unknown error)."
            << std::endl;
        exit(EXIT_FAILURE);
    }

    std::cerr << "\nDetected " << m->getCPUBrandString()
        << " \"Intel(r) microarchitecture codename " << m->getUArchCodename()
        << "\"" << std::endl;

    if (!(m->hasPCICFGUncore())) {
      std::cerr
          << "Jaketown, Ivytown, Haswell, Broadwell-DE Server CPU is required for this tool! Program aborted"
          << std::endl;
      exit(EXIT_FAILURE);
    }

    if (m->getNumSockets() > max_sockets) {
      std::cerr << "Only systems with up to " << max_sockets
          << " sockets are supported! Program aborted" << std::endl;
      exit(EXIT_FAILURE);
    }
    BeforeState = new ServerUncorePowerState[m->getNumSockets()];
    AfterState = new ServerUncorePowerState[m->getNumSockets()];

    BeforeTime = m->getTickCount();
    for (uint32 i = 0; i < m->getNumSockets(); ++i)
      BeforeState[i] = m->getServerUncorePowerState(i);
    event = _event;
    unmask = _unmask;
  }

  void PCM_before_getSystemCounterState() {
    BeforeTime = m->getTickCount();
    for (uint32 i = 0; i < m->getNumSockets(); ++i)
      BeforeState[i] = m->getServerUncorePowerState(i);
  }
  void PCM_after_getSystemCounterState() {
    AfterTime = m->getTickCount();
    for (uint32 i = 0; i < m->getNumSockets(); ++i)
      AfterState[i] = m->getServerUncorePowerState(i);
  }
  void programIMC() {
    MCCntConfig[0] = MC_CH_PCI_PMON_CTL_EVENT(event[0])
        + MC_CH_PCI_PMON_CTL_UMASK(unmask[0]);
    MCCntConfig[1] = MC_CH_PCI_PMON_CTL_EVENT(event[0])
        + MC_CH_PCI_PMON_CTL_UMASK(unmask[1]);

    m->server_pcicfg_uncore[0]->programIMC(MCCntConfig);
    m->server_pcicfg_uncore[1]->programIMC(MCCntConfig);
  }
  void analysis_iMC();
  void display_count(float iMC_socket_chan[][4][8], float iMC_socket[][4],
                     uint32 numSockets, uint32 num_imc_channels);

  ~PCM_MemoryActivities() {
    delete[] BeforeState;
    delete[] AfterState;
    m->cleanup();
  }

 private:
  PCM * m;
  ServerUncorePowerState * BeforeState;
  ServerUncorePowerState * AfterState;
  uint64 BeforeTime, AfterTime = 0;
  unsigned char * event;
  unsigned char * unmask;

  uint32 MCCntConfig[4] = { };  // for adding event
};

class PCM_IPCManager {
 private:
  int shmid;
  void *shared_memory;
  int skey;
  int *ipc_signal;
  int user_identity;

 public:
  PCM_IPCManager(int _user_identity)
      : shmid(0),
        shared_memory((void*) 0),
        skey(9148),
        ipc_signal(0),
        user_identity(_user_identity) {

    switch (user_identity) {
      case PCM_SERVER:
        shmid = shmget((key_t) skey, sizeof(int), 0666 | IPC_CREAT);
        if (shmid == -1) {
          std::cerr << "shmget failed : " << strerror(errno) << std::endl;
          exit(0);
        }
        std::cout << "shm id: " << shmid << std::endl;

        shared_memory = shmat(shmid, (void*) 0, 0);
        if (!shared_memory) {
          std::cerr << "shmat failed : " << strerror(errno) << std::endl;
          exit(0);
        }
        std::cout << "shmat success" << std::endl;
        ipc_signal = (int *) shared_memory;
        *ipc_signal = 100;
        break;
      case PCM_CLIENT:
        shmid = shmget((key_t) skey, sizeof(int), 0);
        if (shmid == -1) {
          std::cerr << "shmget failed : " << strerror(errno) << std::endl;
          exit(0);
        }
        shared_memory = shmat(shmid, (void *) 0, 0666 | IPC_CREAT);
        if (shared_memory == (void *) -1) {
          std::cerr << "shmat failed : " << strerror(errno) << std::endl;
          exit(0);
        }
        ipc_signal = (int *) shared_memory;
        break;
      default:
        break;
    }
  }
  ~PCM_IPCManager() {
  }
  void pcm_satrt() {
    *ipc_signal = WAIT_OFF;
    std::cout << "Start profiling" << std::endl;
  }
  void pcm_stop() {
    *ipc_signal = WAIT_OFF;
    std::cout << "Stop profiling" << std::endl;
  }
  void pcm_wait(int delay) {
    while (*ipc_signal != WAIT_OFF) {
      printf("%d\r", *ipc_signal);
      usleep(delay * 1000);
    }
    *ipc_signal = WAIT_ON;
  }

};

#endif /* PCM_MEMORY_DETAIL_H_ */
