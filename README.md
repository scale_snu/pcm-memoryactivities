## Memory Activity profiler based on [PCM](https://github.com/opcm/pcm)

This application uses PCM library to count memory events (e.g., CAS\_COUNT, etc.) and support two modes ('monitor mode' and 'capture mode').

**Monitor mode**: Count specific memory events in real-time.

**Capture mode**: Enable user to profile during the execution of the specific code.

## Build

```
$ git clone https://github.com/opcm/pcm.git
$ make lib
$ make
```

## Run

```
$ ./pcm-memoryActivities <options>
```

Additional command line flags can be found with `-h`

Options

* `-M` Run as monitor mode. (with `-i <interval>`)
* `-C` Run as capture mode. 
* `-e` specific event name. (with `-u <unmask name>`)


An example:

```
$ sudo ./pcm-memoryActivities -M -i 1.5 -e RD_CAS_RANK0 -u BANK0 -e RD_CAS_RANK0 -u BANK1
-----------------------------------------------||-----------------------------------------------
--                 Socket 0                  --||--                 Socket 1                  --
-----------------------------------------------||-----------------------------------------------
-----------------------------------------------||-----------------------------------------------
-----------------------------------------------||-----------------------------------------------
--       Memory Performance Monitoring       --||--       Memory Performance Monitoring       --
-----------------------------------------------||-----------------------------------------------
--  Mem Ch 0: BANK0 : Count (C/s):  153.00   --||--  Mem Ch 0: BANK0 : Count (C/s):   26.00   --
--  Mem Ch 0: BANK1 : Count (C/s):   56.00   --||--  Mem Ch 0: BANK1 : Count (C/s):    0.00   --
--  Mem Ch 1: BANK0 : Count (C/s): 3173.00   --||--  Mem Ch 1: BANK0 : Count (C/s):   20.00   --
--  Mem Ch 1: BANK1 : Count (C/s): 2160.00   --||--  Mem Ch 1: BANK1 : Count (C/s):    1.00   --
--  Mem Ch 2: BANK0 : Count (C/s):   98.00   --||--  Mem Ch 2: BANK0 : Count (C/s):   26.00   --
--  Mem Ch 2: BANK1 : Count (C/s):  100.00   --||--  Mem Ch 2: BANK1 : Count (C/s):    1.00   --
--  Mem Ch 3: BANK0 : Count (C/s): 3041.00   --||--  Mem Ch 3: BANK0 : Count (C/s):   25.00   --
--  Mem Ch 3: BANK1 : Count (C/s): 2124.00   --||--  Mem Ch 3: BANK1 : Count (C/s):    0.00   --
```
	
## Usage Example: Focus on Specific Code Section

The `pcm_start()` and `pcm_stop()` calls enable you to focus the collection on a specific section of code. 

Example code: example\_IPC\_client.cpp

```
#include "pcm-memoryActivities.h"

int main() {
  PCM_IPCManager pcmIPC(PCM_CLIENT);
  pcmIPC.pcm_satrt();
  
  // kernel
  
  pcmIPC.pcm_stop();
  return 0;
}
```

```
$ ./pcm-memoryActivities -C -e RD_CAS_RANK0 -u BANK0 
$ ./example_IPC_client
```	

## Contact
[Scalable Computer Architecture Laboratory](http://scale.snu.ac.kr/)
